package com.aim.questionnaire.dao.base;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.SurveyAnswer;
/**
*  @author author
*/
public interface SurveyAnswerBaseMapper {

    int insertSurveyAnswer(SurveyAnswer object);

    int updateSurveyAnswer(SurveyAnswer object);

    int update(SurveyAnswer.UpdateBuilder object);

    List<SurveyAnswer> querySurveyAnswer(SurveyAnswer object);

    SurveyAnswer querySurveyAnswerLimit1(SurveyAnswer object);

}
