package com.aim.questionnaire.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.aim.questionnaire.dao.entity.ProjectData;
import com.aim.questionnaire.dao.base.ProjectDataBaseMapper;
/**
*  @author author
*/
public interface ProjectDataMapper extends ProjectDataBaseMapper{

    int deleteProjectData(@Param("id") String id);
}
