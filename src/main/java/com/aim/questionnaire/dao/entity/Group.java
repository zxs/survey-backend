package com.aim.questionnaire.dao.entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
/**
*
*  @author author
*/
public class Group implements Serializable {

    private static final long serialVersionUID = 1667374747390L;


    /**
    * 主键
    * 
    * isNullAble:0
    */
    private String id;

    /**
    * 
    * isNullAble:1
    */
    private String group_name;

    /**
    * 
    * isNullAble:1
    */
    private String user_id;

    /**
    * 
    * isNullAble:1
    */
    private String grop_description;

    /**
    * 
    * isNullAble:1
    */
    private String status;

    /**
    * 
    * isNullAble:1
    */
    private String tenant_id;


    public void setId(String id){this.id = id;}

    public String getId(){return this.id;}

    public void setGroup_name(String group_name){this.group_name = group_name;}

    public String getGroup_name(){return this.group_name;}

    public void setUser_id(String user_id){this.user_id = user_id;}

    public String getUser_id(){return this.user_id;}

    public void setGrop_description(String grop_description){this.grop_description = grop_description;}

    public String getGrop_description(){return this.grop_description;}

    public void setStatus(String status){this.status = status;}

    public String getStatus(){return this.status;}

    public void setTenant_id(String tenant_id){this.tenant_id = tenant_id;}

    public String getTenant_id(){return this.tenant_id;}
    @Override
    public String toString() {
        return "Group{" +
                "id='" + id + '\'' +
                "group_name='" + group_name + '\'' +
                "user_id='" + user_id + '\'' +
                "grop_description='" + grop_description + '\'' +
                "status='" + status + '\'' +
                "tenant_id='" + tenant_id + '\'' +
            '}';
    }

    public static Builder Build(){return new Builder();}

    public static ConditionBuilder ConditionBuild(){return new ConditionBuilder();}

    public static UpdateBuilder UpdateBuild(){return new UpdateBuilder();}

    public static QueryBuilder QueryBuild(){return new QueryBuilder();}

    public static class UpdateBuilder {

        private Group set;

        private ConditionBuilder where;

        public UpdateBuilder set(Group set){
            this.set = set;
            return this;
        }

        public Group getSet(){
            return this.set;
        }

        public UpdateBuilder where(ConditionBuilder where){
            this.where = where;
            return this;
        }

        public ConditionBuilder getWhere(){
            return this.where;
        }

        public UpdateBuilder build(){
            return this;
        }
    }

    public static class QueryBuilder extends Group{
        /**
        * 需要返回的列
        */
        private Map<String,Object> fetchFields;

        public Map<String,Object> getFetchFields(){return this.fetchFields;}

        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> group_nameList;

        public List<String> getGroup_nameList(){return this.group_nameList;}


        private List<String> fuzzyGroup_name;

        public List<String> getFuzzyGroup_name(){return this.fuzzyGroup_name;}

        private List<String> rightFuzzyGroup_name;

        public List<String> getRightFuzzyGroup_name(){return this.rightFuzzyGroup_name;}
        private List<String> user_idList;

        public List<String> getUser_idList(){return this.user_idList;}


        private List<String> fuzzyUser_id;

        public List<String> getFuzzyUser_id(){return this.fuzzyUser_id;}

        private List<String> rightFuzzyUser_id;

        public List<String> getRightFuzzyUser_id(){return this.rightFuzzyUser_id;}
        private List<String> grop_descriptionList;

        public List<String> getGrop_descriptionList(){return this.grop_descriptionList;}


        private List<String> fuzzyGrop_description;

        public List<String> getFuzzyGrop_description(){return this.fuzzyGrop_description;}

        private List<String> rightFuzzyGrop_description;

        public List<String> getRightFuzzyGrop_description(){return this.rightFuzzyGrop_description;}
        private List<String> statusList;

        public List<String> getStatusList(){return this.statusList;}


        private List<String> fuzzyStatus;

        public List<String> getFuzzyStatus(){return this.fuzzyStatus;}

        private List<String> rightFuzzyStatus;

        public List<String> getRightFuzzyStatus(){return this.rightFuzzyStatus;}
        private List<String> tenant_idList;

        public List<String> getTenant_idList(){return this.tenant_idList;}


        private List<String> fuzzyTenant_id;

        public List<String> getFuzzyTenant_id(){return this.fuzzyTenant_id;}

        private List<String> rightFuzzyTenant_id;

        public List<String> getRightFuzzyTenant_id(){return this.rightFuzzyTenant_id;}
        private QueryBuilder (){
            this.fetchFields = new HashMap<>();
        }

        public QueryBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public QueryBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public QueryBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public QueryBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public QueryBuilder id(String id){
            setId(id);
            return this;
        }

        public QueryBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public QueryBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public QueryBuilder fetchId(){
            setFetchFields("fetchFields","id");
            return this;
        }

        public QueryBuilder excludeId(){
            setFetchFields("excludeFields","id");
            return this;
        }

        public QueryBuilder fuzzyGroup_name (List<String> fuzzyGroup_name){
            this.fuzzyGroup_name = fuzzyGroup_name;
            return this;
        }

        public QueryBuilder fuzzyGroup_name (String ... fuzzyGroup_name){
            this.fuzzyGroup_name = solveNullList(fuzzyGroup_name);
            return this;
        }

        public QueryBuilder rightFuzzyGroup_name (List<String> rightFuzzyGroup_name){
            this.rightFuzzyGroup_name = rightFuzzyGroup_name;
            return this;
        }

        public QueryBuilder rightFuzzyGroup_name (String ... rightFuzzyGroup_name){
            this.rightFuzzyGroup_name = solveNullList(rightFuzzyGroup_name);
            return this;
        }

        public QueryBuilder group_name(String group_name){
            setGroup_name(group_name);
            return this;
        }

        public QueryBuilder group_nameList(String ... group_name){
            this.group_nameList = solveNullList(group_name);
            return this;
        }

        public QueryBuilder group_nameList(List<String> group_name){
            this.group_nameList = group_name;
            return this;
        }

        public QueryBuilder fetchGroup_name(){
            setFetchFields("fetchFields","group_name");
            return this;
        }

        public QueryBuilder excludeGroup_name(){
            setFetchFields("excludeFields","group_name");
            return this;
        }

        public QueryBuilder fuzzyUser_id (List<String> fuzzyUser_id){
            this.fuzzyUser_id = fuzzyUser_id;
            return this;
        }

        public QueryBuilder fuzzyUser_id (String ... fuzzyUser_id){
            this.fuzzyUser_id = solveNullList(fuzzyUser_id);
            return this;
        }

        public QueryBuilder rightFuzzyUser_id (List<String> rightFuzzyUser_id){
            this.rightFuzzyUser_id = rightFuzzyUser_id;
            return this;
        }

        public QueryBuilder rightFuzzyUser_id (String ... rightFuzzyUser_id){
            this.rightFuzzyUser_id = solveNullList(rightFuzzyUser_id);
            return this;
        }

        public QueryBuilder user_id(String user_id){
            setUser_id(user_id);
            return this;
        }

        public QueryBuilder user_idList(String ... user_id){
            this.user_idList = solveNullList(user_id);
            return this;
        }

        public QueryBuilder user_idList(List<String> user_id){
            this.user_idList = user_id;
            return this;
        }

        public QueryBuilder fetchUser_id(){
            setFetchFields("fetchFields","user_id");
            return this;
        }

        public QueryBuilder excludeUser_id(){
            setFetchFields("excludeFields","user_id");
            return this;
        }

        public QueryBuilder fuzzyGrop_description (List<String> fuzzyGrop_description){
            this.fuzzyGrop_description = fuzzyGrop_description;
            return this;
        }

        public QueryBuilder fuzzyGrop_description (String ... fuzzyGrop_description){
            this.fuzzyGrop_description = solveNullList(fuzzyGrop_description);
            return this;
        }

        public QueryBuilder rightFuzzyGrop_description (List<String> rightFuzzyGrop_description){
            this.rightFuzzyGrop_description = rightFuzzyGrop_description;
            return this;
        }

        public QueryBuilder rightFuzzyGrop_description (String ... rightFuzzyGrop_description){
            this.rightFuzzyGrop_description = solveNullList(rightFuzzyGrop_description);
            return this;
        }

        public QueryBuilder grop_description(String grop_description){
            setGrop_description(grop_description);
            return this;
        }

        public QueryBuilder grop_descriptionList(String ... grop_description){
            this.grop_descriptionList = solveNullList(grop_description);
            return this;
        }

        public QueryBuilder grop_descriptionList(List<String> grop_description){
            this.grop_descriptionList = grop_description;
            return this;
        }

        public QueryBuilder fetchGrop_description(){
            setFetchFields("fetchFields","grop_description");
            return this;
        }

        public QueryBuilder excludeGrop_description(){
            setFetchFields("excludeFields","grop_description");
            return this;
        }

        public QueryBuilder fuzzyStatus (List<String> fuzzyStatus){
            this.fuzzyStatus = fuzzyStatus;
            return this;
        }

        public QueryBuilder fuzzyStatus (String ... fuzzyStatus){
            this.fuzzyStatus = solveNullList(fuzzyStatus);
            return this;
        }

        public QueryBuilder rightFuzzyStatus (List<String> rightFuzzyStatus){
            this.rightFuzzyStatus = rightFuzzyStatus;
            return this;
        }

        public QueryBuilder rightFuzzyStatus (String ... rightFuzzyStatus){
            this.rightFuzzyStatus = solveNullList(rightFuzzyStatus);
            return this;
        }

        public QueryBuilder status(String status){
            setStatus(status);
            return this;
        }

        public QueryBuilder statusList(String ... status){
            this.statusList = solveNullList(status);
            return this;
        }

        public QueryBuilder statusList(List<String> status){
            this.statusList = status;
            return this;
        }

        public QueryBuilder fetchStatus(){
            setFetchFields("fetchFields","status");
            return this;
        }

        public QueryBuilder excludeStatus(){
            setFetchFields("excludeFields","status");
            return this;
        }

        public QueryBuilder fuzzyTenant_id (List<String> fuzzyTenant_id){
            this.fuzzyTenant_id = fuzzyTenant_id;
            return this;
        }

        public QueryBuilder fuzzyTenant_id (String ... fuzzyTenant_id){
            this.fuzzyTenant_id = solveNullList(fuzzyTenant_id);
            return this;
        }

        public QueryBuilder rightFuzzyTenant_id (List<String> rightFuzzyTenant_id){
            this.rightFuzzyTenant_id = rightFuzzyTenant_id;
            return this;
        }

        public QueryBuilder rightFuzzyTenant_id (String ... rightFuzzyTenant_id){
            this.rightFuzzyTenant_id = solveNullList(rightFuzzyTenant_id);
            return this;
        }

        public QueryBuilder tenant_id(String tenant_id){
            setTenant_id(tenant_id);
            return this;
        }

        public QueryBuilder tenant_idList(String ... tenant_id){
            this.tenant_idList = solveNullList(tenant_id);
            return this;
        }

        public QueryBuilder tenant_idList(List<String> tenant_id){
            this.tenant_idList = tenant_id;
            return this;
        }

        public QueryBuilder fetchTenant_id(){
            setFetchFields("fetchFields","tenant_id");
            return this;
        }

        public QueryBuilder excludeTenant_id(){
            setFetchFields("excludeFields","tenant_id");
            return this;
        }
        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public QueryBuilder fetchAll(){
            this.fetchFields.put("AllFields",true);
            return this;
        }

        public QueryBuilder addField(String ... fields){
            List<String> list = new ArrayList<>();
            if (fields != null){
                for (String field : fields){
                    list.add(field);
                }
            }
            this.fetchFields.put("otherFields",list);
            return this;
        }
        @SuppressWarnings("unchecked")
        private void setFetchFields(String key,String val){
            Map<String,Boolean> fields= (Map<String, Boolean>) this.fetchFields.get(key);
            if (fields == null){
                fields = new HashMap<>();
            }
            fields.put(val,true);
            this.fetchFields.put(key,fields);
        }

        public Group build(){return this;}
    }


    public static class ConditionBuilder{
        private List<String> idList;

        public List<String> getIdList(){return this.idList;}


        private List<String> fuzzyId;

        public List<String> getFuzzyId(){return this.fuzzyId;}

        private List<String> rightFuzzyId;

        public List<String> getRightFuzzyId(){return this.rightFuzzyId;}
        private List<String> group_nameList;

        public List<String> getGroup_nameList(){return this.group_nameList;}


        private List<String> fuzzyGroup_name;

        public List<String> getFuzzyGroup_name(){return this.fuzzyGroup_name;}

        private List<String> rightFuzzyGroup_name;

        public List<String> getRightFuzzyGroup_name(){return this.rightFuzzyGroup_name;}
        private List<String> user_idList;

        public List<String> getUser_idList(){return this.user_idList;}


        private List<String> fuzzyUser_id;

        public List<String> getFuzzyUser_id(){return this.fuzzyUser_id;}

        private List<String> rightFuzzyUser_id;

        public List<String> getRightFuzzyUser_id(){return this.rightFuzzyUser_id;}
        private List<String> grop_descriptionList;

        public List<String> getGrop_descriptionList(){return this.grop_descriptionList;}


        private List<String> fuzzyGrop_description;

        public List<String> getFuzzyGrop_description(){return this.fuzzyGrop_description;}

        private List<String> rightFuzzyGrop_description;

        public List<String> getRightFuzzyGrop_description(){return this.rightFuzzyGrop_description;}
        private List<String> statusList;

        public List<String> getStatusList(){return this.statusList;}


        private List<String> fuzzyStatus;

        public List<String> getFuzzyStatus(){return this.fuzzyStatus;}

        private List<String> rightFuzzyStatus;

        public List<String> getRightFuzzyStatus(){return this.rightFuzzyStatus;}
        private List<String> tenant_idList;

        public List<String> getTenant_idList(){return this.tenant_idList;}


        private List<String> fuzzyTenant_id;

        public List<String> getFuzzyTenant_id(){return this.fuzzyTenant_id;}

        private List<String> rightFuzzyTenant_id;

        public List<String> getRightFuzzyTenant_id(){return this.rightFuzzyTenant_id;}

        public ConditionBuilder fuzzyId (List<String> fuzzyId){
            this.fuzzyId = fuzzyId;
            return this;
        }

        public ConditionBuilder fuzzyId (String ... fuzzyId){
            this.fuzzyId = solveNullList(fuzzyId);
            return this;
        }

        public ConditionBuilder rightFuzzyId (List<String> rightFuzzyId){
            this.rightFuzzyId = rightFuzzyId;
            return this;
        }

        public ConditionBuilder rightFuzzyId (String ... rightFuzzyId){
            this.rightFuzzyId = solveNullList(rightFuzzyId);
            return this;
        }

        public ConditionBuilder idList(String ... id){
            this.idList = solveNullList(id);
            return this;
        }

        public ConditionBuilder idList(List<String> id){
            this.idList = id;
            return this;
        }

        public ConditionBuilder fuzzyGroup_name (List<String> fuzzyGroup_name){
            this.fuzzyGroup_name = fuzzyGroup_name;
            return this;
        }

        public ConditionBuilder fuzzyGroup_name (String ... fuzzyGroup_name){
            this.fuzzyGroup_name = solveNullList(fuzzyGroup_name);
            return this;
        }

        public ConditionBuilder rightFuzzyGroup_name (List<String> rightFuzzyGroup_name){
            this.rightFuzzyGroup_name = rightFuzzyGroup_name;
            return this;
        }

        public ConditionBuilder rightFuzzyGroup_name (String ... rightFuzzyGroup_name){
            this.rightFuzzyGroup_name = solveNullList(rightFuzzyGroup_name);
            return this;
        }

        public ConditionBuilder group_nameList(String ... group_name){
            this.group_nameList = solveNullList(group_name);
            return this;
        }

        public ConditionBuilder group_nameList(List<String> group_name){
            this.group_nameList = group_name;
            return this;
        }

        public ConditionBuilder fuzzyUser_id (List<String> fuzzyUser_id){
            this.fuzzyUser_id = fuzzyUser_id;
            return this;
        }

        public ConditionBuilder fuzzyUser_id (String ... fuzzyUser_id){
            this.fuzzyUser_id = solveNullList(fuzzyUser_id);
            return this;
        }

        public ConditionBuilder rightFuzzyUser_id (List<String> rightFuzzyUser_id){
            this.rightFuzzyUser_id = rightFuzzyUser_id;
            return this;
        }

        public ConditionBuilder rightFuzzyUser_id (String ... rightFuzzyUser_id){
            this.rightFuzzyUser_id = solveNullList(rightFuzzyUser_id);
            return this;
        }

        public ConditionBuilder user_idList(String ... user_id){
            this.user_idList = solveNullList(user_id);
            return this;
        }

        public ConditionBuilder user_idList(List<String> user_id){
            this.user_idList = user_id;
            return this;
        }

        public ConditionBuilder fuzzyGrop_description (List<String> fuzzyGrop_description){
            this.fuzzyGrop_description = fuzzyGrop_description;
            return this;
        }

        public ConditionBuilder fuzzyGrop_description (String ... fuzzyGrop_description){
            this.fuzzyGrop_description = solveNullList(fuzzyGrop_description);
            return this;
        }

        public ConditionBuilder rightFuzzyGrop_description (List<String> rightFuzzyGrop_description){
            this.rightFuzzyGrop_description = rightFuzzyGrop_description;
            return this;
        }

        public ConditionBuilder rightFuzzyGrop_description (String ... rightFuzzyGrop_description){
            this.rightFuzzyGrop_description = solveNullList(rightFuzzyGrop_description);
            return this;
        }

        public ConditionBuilder grop_descriptionList(String ... grop_description){
            this.grop_descriptionList = solveNullList(grop_description);
            return this;
        }

        public ConditionBuilder grop_descriptionList(List<String> grop_description){
            this.grop_descriptionList = grop_description;
            return this;
        }

        public ConditionBuilder fuzzyStatus (List<String> fuzzyStatus){
            this.fuzzyStatus = fuzzyStatus;
            return this;
        }

        public ConditionBuilder fuzzyStatus (String ... fuzzyStatus){
            this.fuzzyStatus = solveNullList(fuzzyStatus);
            return this;
        }

        public ConditionBuilder rightFuzzyStatus (List<String> rightFuzzyStatus){
            this.rightFuzzyStatus = rightFuzzyStatus;
            return this;
        }

        public ConditionBuilder rightFuzzyStatus (String ... rightFuzzyStatus){
            this.rightFuzzyStatus = solveNullList(rightFuzzyStatus);
            return this;
        }

        public ConditionBuilder statusList(String ... status){
            this.statusList = solveNullList(status);
            return this;
        }

        public ConditionBuilder statusList(List<String> status){
            this.statusList = status;
            return this;
        }

        public ConditionBuilder fuzzyTenant_id (List<String> fuzzyTenant_id){
            this.fuzzyTenant_id = fuzzyTenant_id;
            return this;
        }

        public ConditionBuilder fuzzyTenant_id (String ... fuzzyTenant_id){
            this.fuzzyTenant_id = solveNullList(fuzzyTenant_id);
            return this;
        }

        public ConditionBuilder rightFuzzyTenant_id (List<String> rightFuzzyTenant_id){
            this.rightFuzzyTenant_id = rightFuzzyTenant_id;
            return this;
        }

        public ConditionBuilder rightFuzzyTenant_id (String ... rightFuzzyTenant_id){
            this.rightFuzzyTenant_id = solveNullList(rightFuzzyTenant_id);
            return this;
        }

        public ConditionBuilder tenant_idList(String ... tenant_id){
            this.tenant_idList = solveNullList(tenant_id);
            return this;
        }

        public ConditionBuilder tenant_idList(List<String> tenant_id){
            this.tenant_idList = tenant_id;
            return this;
        }

        private <T>List<T> solveNullList(T ... objs){
            if (objs != null){
            List<T> list = new ArrayList<>();
                for (T item : objs){
                    if (item != null){
                        list.add(item);
                    }
                }
                return list;
            }
            return null;
        }

        public ConditionBuilder build(){return this;}
    }

    public static class Builder {

        private Group obj;

        public Builder(){
            this.obj = new Group();
        }

        public Builder id(String id){
            this.obj.setId(id);
            return this;
        }
        public Builder group_name(String group_name){
            this.obj.setGroup_name(group_name);
            return this;
        }
        public Builder user_id(String user_id){
            this.obj.setUser_id(user_id);
            return this;
        }
        public Builder grop_description(String grop_description){
            this.obj.setGrop_description(grop_description);
            return this;
        }
        public Builder status(String status){
            this.obj.setStatus(status);
            return this;
        }
        public Builder tenant_id(String tenant_id){
            this.obj.setTenant_id(tenant_id);
            return this;
        }
        public Group build(){return obj;}
    }

}
