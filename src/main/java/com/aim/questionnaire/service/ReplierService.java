package com.aim.questionnaire.service;

import com.aim.questionnaire.common.Role;
import com.aim.questionnaire.common.utils.UUIDUtil;
import com.aim.questionnaire.dao.AnswerMapper;
import com.aim.questionnaire.dao.TotalRoleMapper;
import com.aim.questionnaire.dao.entity.Answer;
import com.aim.questionnaire.dao.entity.TotalRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ReplierService {
    @Autowired
    private AnswerMapper answerMapper;

    @Autowired
    private TotalRoleMapper totalRoleMapper;

    public Boolean addReplier(Map<String, Object> map) {
        String account = (String) map.get("account");
        String password = (String) map.get("password");
        String phoneNumber = (String) map.get("phoneNumber");
        String groupId = (String) map.get("groupId");

        String id = UUIDUtil.getOneUUID();
        Answer replier = Answer.Build()
                .id(id)
                .group_id(groupId)
                .build();
        answerMapper.insertAnswer(replier);

        TotalRole totalRole = TotalRole.Build()
                .id(id)
                .account(account)
                .password(password)
                .role(Role.REPLIER.ordinal())
                .phone_number(phoneNumber)
                .status(1)
                .security_question("")
                .security_answer("")
                .deleted(0)
                .build();
        totalRoleMapper.insertTotalRole(totalRole);
        return true;
    }

    public Boolean addMultiReplierByEx(Map<String, Object> map) {
        return true;
    }

    public Boolean addMultiReplierById(Map<String, Object> map) {
        return true;
    }

    public List<Object> queryReplier(String groupId, String phoneNumber) {
        Answer query_answer = Answer.QueryBuild()
                .group_id(groupId)
                .fetchAll()
                .build();
        List<Answer> answerList = answerMapper.queryAnswer(query_answer);
        TotalRole query_total = TotalRole.QueryBuild()
                .phone_number(phoneNumber)
                .role(Role.REPLIER.ordinal())
                .fetchAll()
                .build();
        List<TotalRole> totalRoleList = totalRoleMapper.queryTotalRole(query_total);

        List res = new ArrayList();
        for (Answer a : answerList) {
            for (TotalRole b : totalRoleList) {
                if (a.getId().equals(b.getId())) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("id", a.getId());
                    map.put("groupId", a.getGroup_id());
                    map.put("account", b.getAccount());
                    map.put("phoneNumber", b.getPhone_number());
                    map.put("status", b.getStatus());
                    map.put("securityQuestion", b.getSecurity_question());
                    map.put("securityAnswer", b.getSecurity_answer());
                    map.put("deleted", b.getDeleted());
                    res.add(map);
                }
            }
        }
        return res;
    }

    public List<Object> queryReplierList(ArrayList<String> groupIdList, String phoneNumber) {
        String groupId = groupIdList.get(0);
        Answer query_answer = Answer.QueryBuild()
                .group_id(groupId)
                .fetchAll()
                .build();
        List<Answer> answerList = answerMapper.queryAnswer(query_answer);
        TotalRole query_total = TotalRole.QueryBuild()
                .phone_number(phoneNumber)
                .role(Role.REPLIER.ordinal())
                .fetchAll()
                .build();
        List<TotalRole> totalRoleList = totalRoleMapper.queryTotalRole(query_total);

        List res = new ArrayList();
        for (Answer a : answerList) {
            for (TotalRole b : totalRoleList) {
                if (a.getId().equals(b.getId())) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("id", a.getId());
                    map.put("groupId", a.getGroup_id());
                    map.put("account", b.getAccount());
                    map.put("phoneNumber", b.getPhone_number());
                    map.put("status", b.getStatus());
                    map.put("securityQuestion", b.getSecurity_question());
                    map.put("securityAnswer", b.getSecurity_answer());
                    map.put("deleted", b.getDeleted());
                    res.add(map);
                }
            }
        }
        return res;
    }


    public Boolean modifyReplier(Map<String, Object> map) {
        String id = (String) map.get("id");
        String password = (String) map.get("password");
        String phoneNumber = (String) map.get("phoneNumber");
        String securityQuestion = (String) map.get("securityQuestion");
        String securityAnswer = (String) map.get("securityAnswer");
        /*Answer replier =
                Answer.Build()
                        .id(id)
                        .group_id(null)
                        .build();
        answerMapper.updateAnswer(replier);*/

        TotalRole totalRole = TotalRole.Build()
                .id(id)
                .password(password)
                .phone_number(phoneNumber)
                .security_answer(securityAnswer)
                .security_question(securityQuestion)
                .build();
        totalRoleMapper.updateTotalRole(totalRole);
        return true;
    }

    public Boolean deleteReplier(String id) {
        TotalRole totalRole = TotalRole.Build()
                .id(id)
                .deleted(1)
                .build();
        totalRoleMapper.updateTotalRole(totalRole);
        return true;
    }

}
