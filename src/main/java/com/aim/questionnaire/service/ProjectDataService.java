package com.aim.questionnaire.service;

import com.aim.questionnaire.common.utils.UUIDUtil;
import com.aim.questionnaire.dao.ProjectDataMapper;
import com.aim.questionnaire.dao.SurveyLinkedMapper;
import com.aim.questionnaire.dao.entity.ProjectData;
import com.aim.questionnaire.dao.entity.SurveyLinked;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ProjectDataService {

    @Autowired
    private ProjectDataMapper projectDataMapper;

    @Autowired
    private SurveyLinkedMapper surveyLinkedMapper;

    public int addProject(ProjectData projectData) {
        projectData.setId(UUIDUtil.getOneUUID());
        projectData.setProjectStatus("closed");
        projectDataMapper.insertProjectData(projectData);
        return 0;
    }

    public List<ProjectData> queryProjectList(String createdBy, String projectName) {
        ProjectData query = ProjectData.queryBuild().createdBy(createdBy).projectName(projectName).fetchAll().build();
        return projectDataMapper.queryProjectData(query);
    }

    public int deleteProject(String id) {
        SurveyLinked query = SurveyLinked.queryBuild().projectId(id).fetchSurveyStatus().build();
        List<SurveyLinked> list = surveyLinkedMapper.querySurveyLinked(query);
        for (SurveyLinked survey : list) {
            if (survey.getSurveyStatus() != 0) {
                return -1;
            }
        }
        projectDataMapper.deleteProjectData(id);
        return 0;
    }

    public int modifyProject(Map<String, Object> map) {
        String id = (String) map.get("id");
        String projectName = (String) map.get("projectName");
        String projectDescription = (String) map.get("projectDescription");
        ProjectData projectData =
                ProjectData.bBuild()
                        .id(id)
                        .projectName(projectName)
                        .projectDescription(projectDescription)
                        .build();
        projectDataMapper.updateProjectData(projectData);
        return 0;
    }
}
