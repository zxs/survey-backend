package com.aim.questionnaire.service;

import com.aim.questionnaire.dao.TenantMapper;
import com.aim.questionnaire.dao.TotalRoleMapper;
import com.aim.questionnaire.dao.entity.Tenant;
import com.aim.questionnaire.dao.entity.TotalRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class TenantDataService {
    @Autowired
    private TenantMapper tenantMapper;

    @Autowired
    private TotalRoleMapper totalRoleMapper;


    public List<Object> queryTenantList(String phonenumber, String account){
        TotalRole totalRole = TotalRole.QueryBuild().phone_number(phonenumber).account(account).fetchAll().build();
        List<TotalRole> a = totalRoleMapper.queryTotalRole(totalRole);
        List<Object> arrayList = new ArrayList();
        for(TotalRole arr : a){
            Map<String,Object> map = new HashMap<>();
            String m = arr.getId();
            Tenant tenant = Tenant.QueryBuild().id(m).fetchAll().build();
            Tenant n  = tenantMapper.queryTenantLimit1(tenant);
            map.put("id",m);
            map.put("account",arr.getAccount());
            map.put("phoneNumber",arr.getPhone_number());
            map.put("bill",n.getBill());
            map.put("status",arr.getStatus());
            map.put("securityQustion",arr.getSecurity_question());
            map.put("securityAnswer",arr.getSecurity_answer());
            map.put("deleted",arr.getDeleted());
            arrayList.add(map);
        }
        return arrayList;
    }

    public int deleteTenant(String id){
        TotalRole totalRole = TotalRole.QueryBuild().id(id).fetchStatus().build();
        List<TotalRole> list = totalRoleMapper.queryTotalRole(totalRole);
        for(TotalRole t : list){
            if (t.getStatus()==0){
                return -1;
            }
        }
        totalRole.setDeleted(1);
        return 0;
    }

    public int modifyTenant(Map<String, Object> map) {
        String id = (String) map.get("id");
        String phoneNumber = (String) map.get("phoneNumber");
        String password = (String) map.get("password");
        String securityQuestion = (String) map.get("securityQuestion");
        String securityAnswer = (String) map.get("securityAnswer");
        int deleted = (int) map.get("deleted");
        TotalRole totalRole = TotalRole.Build().id(id)
                .phone_number(phoneNumber).password(password)
                        .security_question(securityQuestion)
                                .security_answer(securityAnswer)
                                        .deleted(deleted).build();

        totalRoleMapper.updateTotalRole(totalRole);
        return 0;
    }
}
