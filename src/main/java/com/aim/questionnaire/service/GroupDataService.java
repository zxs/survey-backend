package com.aim.questionnaire.service;

import com.aim.questionnaire.common.utils.UUIDUtil;
import com.aim.questionnaire.dao.GroupMapper;
import com.aim.questionnaire.dao.TotalRoleMapper;
import com.aim.questionnaire.dao.entity.Group;
import com.aim.questionnaire.dao.entity.Tenant;
import com.aim.questionnaire.dao.entity.TotalRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class GroupDataService {
    @Autowired
    private GroupMapper groupMapper;

    @Autowired
    private TotalRoleMapper totalRoleMapper;

    public int addGroup(Map<String,Object> map){
        Group group = Group.Build().id(UUIDUtil.getOneUUID())
                .group_name((String) map.get("groupName"))
                .user_id((String)map.get("userId"))
                .tenant_id((String)map.get("tenantId")).build();
        groupMapper.insertGroup(group);
        return 0;
    }

    public int deleteGroup(String id){
        TotalRole totalRole = TotalRole.QueryBuild().id(id).build();
        List<TotalRole> list = totalRoleMapper.queryTotalRole(totalRole);
        for(TotalRole t : list){
            if (t.getStatus()==0){
                return -1;
            }
        }
        totalRole.setDeleted(1);
        return 0;
    }

    public int modifyGroup(Map<String, Object> map) {
        String id = (String) map.get("id");
        String groupName = (String) map.get("groupName");
        String groupDescription = (String) map.get("groupDescription");
        int deleted = (int) map.get("deleted");
        Group group = Group.Build().id(id).group_name(groupName).grop_description(groupDescription).build();
        TotalRole totalRole = TotalRole.Build().deleted(deleted).build();
        groupMapper.updateGroup(group);
        totalRoleMapper.updateTotalRole(totalRole);
        return 0;
    }

    public List<Object> queryGroupList(String userId, String tenantId ,String groupName){
        Group group = Group.Build().user_id(userId).tenant_id(tenantId).group_name(groupName).build();
        List<Group> a = groupMapper.queryGroup(group);
        List<Object> arrayList = new ArrayList();
        for(Group arr : a){
            Map<String,Object> map = new HashMap<>();
            String m = arr.getId();
            TotalRole totalRole = TotalRole.Build().id(m).build();
            TotalRole n =totalRoleMapper.queryTotalRoleLimit1(totalRole);
            map.put("id",m);
            map.put("userId",arr.getUser_id());
            map.put("groupName",arr.getGroup_name());
            map.put("groupDescription",arr.getGrop_description());
            map.put("tenantId",arr.getTenant_id());
            map.put("status",arr.getStatus());
            map.put("deleted",n.getDeleted());
            arrayList.add(map);
        }
        return arrayList;
    }
}
