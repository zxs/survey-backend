package com.aim.questionnaire.service;

import com.aim.questionnaire.common.Role;
import com.aim.questionnaire.common.utils.UUIDUtil;
import com.aim.questionnaire.dao.*;
import com.aim.questionnaire.dao.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class LoginAuthService {

    @Autowired
    private TotalRoleMapper totalRoleMapper;

    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private TenantMapper tenantMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private AnswerMapper answerMapper;

    public Map<String, Object> pwLogin(String account, String password) {
        TotalRole query = TotalRole
                .QueryBuild()
                .fetchAll()
                .account(account)
                .build();
        TotalRole res = totalRoleMapper.queryTotalRoleLimit1(query);
        Map<String, Object> map = new HashMap<String, Object>();
        if (res == null) {
            return null;
        }
        if (!res.getPassword().equals(password)) {
            return null;
        }
        map.put("id", res.getId());
        map.put("role", Role.values()[res.getRole()].name().toLowerCase());
        map.put("token", UUIDUtil.getOneUUID());
        return map;
    }

    public Boolean sendSms(String phoneNumber, String purpose) {
        return true;
    }

    public Map<String, Object> smsLogin(String veriCode, String phoneNumber) {
        Map<String, Object> map = new HashMap<>();
        return map;
    }

    public Boolean register(Map<String, Object> map) {
        String account = (String) map.get("account");
        String password = (String) map.get("password");
        String phoneNumber = (String) map.get("phoneNumber");
        String veriCode = (String) map.get("veriCode");
        String role = (String) map.get("role");

        String securityQuestion = (String) map.get("securityQuestion");
        String securityAnswer = new String();
        if (securityQuestion != null) {
            securityAnswer = (String) map.get("securityAnswer");
        }
        String id = UUIDUtil.getOneUUID();
        TotalRole totalRole = TotalRole.Build()
                .id(id)
                .account(account)
                .password(password)
                .phone_number(phoneNumber)
                .role(Role.valueOf(role.toUpperCase()).ordinal())
                .security_question(securityQuestion)
                .security_answer(securityAnswer)
                .deleted(0)
                .build();
        totalRoleMapper.insertTotalRole(totalRole);
        switch (role) {
            case "admin":
                Admin admin = Admin.Build()
                        .id(id)
                        .account(account)
                        .password(password)
                        .build();
                adminMapper.insertAdmin(admin);
                break;
            case "tenant":
                Tenant tenant = Tenant.Build()
                        .id(id)
                        .bill(0.0)
                        .build();
                tenantMapper.insertTenant(tenant);
                break;
            case "user":
                String tenantAccount = (String) map.get("tenantAccount");
                User user = User.Build()
                        .id(id)
                        .tenant_id(tenantAccount)
                        .build();
                userMapper.insertUser(user);
                break;
            case "replier":
                String groupName = (String) map.get("groupName");
                Answer replier = Answer.Build()
                        .id(id)
                        .group_id(groupName)
                        .build();
                answerMapper.insertAnswer(replier);
                break;
            default:
                return false;
        }
        return true;
    }

    public Boolean secRetrieval(Map<String, Object> map) {
        String account = (String) map.get("account");
        String securityAnswer = (String) map.get("securityAnswer");
        String newPassword = (String) map.get("newPassword");

        TotalRole totalRole = TotalRole.QueryBuild()
                .account(account);
        TotalRole res = totalRoleMapper.queryTotalRoleLimit1(totalRole);
        if (!res.getSecurity_answer().equals(securityAnswer)) {
            return false;
        }
        return true;
    }

    public Boolean smsRetrieval(Map<String, Object> map) {
        String phoneNumber = (String) map.get("phoneNumber");
        String veriCode = (String) map.get("veriCode");
        String newPassword = (String) map.get("newPassword");
        return true;
    }
}
