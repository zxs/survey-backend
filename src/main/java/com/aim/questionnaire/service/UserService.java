package com.aim.questionnaire.service;

import com.aim.questionnaire.common.Role;
import com.aim.questionnaire.common.utils.UUIDUtil;
import com.aim.questionnaire.dao.TotalRoleMapper;
import com.aim.questionnaire.dao.UserMapper;
import com.aim.questionnaire.dao.entity.TotalRole;
import com.aim.questionnaire.dao.entity.User;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private TotalRoleMapper totalRoleMapper;

    public Boolean addUser(Map<String, Object> map) {
        String account = (String) map.get("account");
        String password = (String) map.get("password");
        String phoneNumber = (String) map.get("phoneNumber");
        String tenantId = (String) map.get("tenantId");

        String id = UUIDUtil.getOneUUID();
        User user = User.Build()
                .id(id)
                .tenant_id(tenantId)
                .build();
        userMapper.insertUser(user);

        TotalRole totalRole = TotalRole.Build()
                .id(id)
                .account(account)
                .password(password)
                .role(Role.USER.ordinal())
                .phone_number(phoneNumber)
                .status(1)
                .security_question("")
                .security_answer("")
                .deleted(0)
                .build();
        totalRoleMapper.insertTotalRole(totalRole);
        return true;
    }

    public Boolean addMultiUserByEx(Map<String, Object> map) {
        return true;
    }

    public List<Object> queryUserList(String tenantId, String phoneNumber) {
        User query_user = User.QueryBuild().tenant_id(tenantId).fetchAll().build();
        List<User> userList = userMapper.queryUser(query_user);
        TotalRole query_total = TotalRole.QueryBuild()
                .phone_number(phoneNumber)
                .role(Role.USER.ordinal())
                .build();
        List<TotalRole> totalRoleList = totalRoleMapper.queryTotalRole(query_total);

        List res = new ArrayList();
        for (User a : userList) {
            for (TotalRole b : totalRoleList) {
                if (a.getId().equals(b.getId())) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("id", a.getId());
                    map.put("tenantId", a.getTenant_id());
                    map.put("account", b.getAccount());
                    map.put("phoneNumber", b.getPhone_number());
                    map.put("status", b.getStatus());
                    map.put("securityQuestion", b.getSecurity_question());
                    map.put("securityAnswer", b.getSecurity_answer());
                    map.put("deleted", b.getDeleted());
                    res.add(map);
                }
            }
        }
        return res;
    }

    public Boolean modifyUser(Map<String, Object> map) {
        String id = (String) map.get("id");
        String tenantId = (String) map.get("tenantId");

        String password = (String) map.get("password");
        String securityQuestion = (String) map.get("securityQuestion");
        String securityAnswer = (String) map.get("securityAnswer");
        User user = User.Build()
                .id(id)
                .tenant_id(tenantId)
                .build();
        userMapper.updateUser(user);
        TotalRole totalRole = TotalRole.Build()
                .id(id)
                .password(password)
                .security_answer(securityAnswer)
                .security_question(securityQuestion)
                .build();
        totalRoleMapper.updateTotalRole(totalRole);
        return true;
    }

    public int deleteUser(String id) {
        TotalRole totalRole = TotalRole.Build()
                .id(id)
                .deleted(1)
                .build();
        totalRoleMapper.updateTotalRole(totalRole);
        return 0;
    }

}
