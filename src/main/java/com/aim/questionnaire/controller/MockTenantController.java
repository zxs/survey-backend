package com.aim.questionnaire.controller;

import com.aim.questionnaire.common.Constans;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MockTenantController {

    @RequestMapping(value = "/addTenant", method = RequestMethod.POST, headers = "Accept=application/json")
    public TenantHttpResponseEntity addTenant(@RequestBody Map<String, Object> map) {
        TenantHttpResponseEntity tenantHttpResponseEntity = new TenantHttpResponseEntity();
        tenantHttpResponseEntity.setCode(Integer.toString(801));
        return tenantHttpResponseEntity;
    }

    @RequestMapping(value = "/queryTenantList", method = RequestMethod.GET, headers = "Accept=application/json")
    public TenantHttpResponseEntity queryTenantList(
            @RequestParam(required = false, name = "userId") String userId,
            @RequestParam(required = false, name = "tenantId") String tenantId,
            @RequestParam(required = false, name = "tenantName") String tenantName) {
        TenantHttpResponseEntity tenantHttpResponseEntity = new TenantHttpResponseEntity();
        try {
            List list = new ArrayList();
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", "00000000000000000000000000000000");
            map.put("account", "tenant0");
            map.put("phoneNumber", "12234");
            map.put("bill", 1.5);
            map.put("authority", "true");
            map.put("status", "true");
            map.put("securityQuestion", "wtf");
            map.put("securityAnswer", "wtf");
            list.add(map);
            tenantHttpResponseEntity.setTenantList(list);
            tenantHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            tenantHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return tenantHttpResponseEntity;
    }

    @RequestMapping(value = "/modifyTenant", method = RequestMethod.PUT, headers = "Accept=application/json")
    public TenantHttpResponseEntity modifyTenant(@RequestBody Map<String, Object> map) {
        TenantHttpResponseEntity tenantHttpResponseEntity = new TenantHttpResponseEntity();
        tenantHttpResponseEntity.setCode(Integer.toString(801));
        return tenantHttpResponseEntity;
    }


    @RequestMapping(value = "/deleteTenant", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public TenantHttpResponseEntity deleteTenant(@RequestParam("id") String id) {
        TenantHttpResponseEntity tenantHttpResponseEntity = new TenantHttpResponseEntity();
        tenantHttpResponseEntity.setCode(Integer.toString(801));
        return tenantHttpResponseEntity;
    }
}

class TenantHttpResponseEntity implements Serializable {

    private String code;
    private Object tenantList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getTenantList() {
        return tenantList;
    }

    public void setTenantList(Object tenantList) {
        this.tenantList = tenantList;
    }
}
