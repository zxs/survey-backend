package com.aim.questionnaire.controller;

import com.aim.questionnaire.common.Constans;
import com.aim.questionnaire.dao.entity.ProjectData;
import com.aim.questionnaire.service.ProjectDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@RestController
public class ProjectDataController {

    @Autowired
    private ProjectDataService projectDataService;

    @RequestMapping(value = "/addProject", method = RequestMethod.POST, headers = "Accept=application/json")
    public ProjectHttpResponseEntity addProject(@RequestBody ProjectData projectData) {
        ProjectHttpResponseEntity projectHttpResponseEntity = new ProjectHttpResponseEntity();
        try {
            projectDataService.addProject(projectData);
            projectHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            projectHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return projectHttpResponseEntity;
    }


    @RequestMapping(value = "/queryProjectList", method = RequestMethod.GET, headers = "Accept=application/json")
    public ProjectHttpResponseEntity
    queryProjectList(@RequestParam(required = true, name = "createdBy") String createdBy,
                     @RequestParam(required = false, name = "projectName") String projectName) {
        ProjectHttpResponseEntity projectHttpResponseEntity = new ProjectHttpResponseEntity();
        try {
            List res = projectDataService.queryProjectList(createdBy, projectName);
            projectHttpResponseEntity.setProjectList(res);
            projectHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            projectHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return projectHttpResponseEntity;
    }

    @RequestMapping(value = "/deleteProject", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public ProjectHttpResponseEntity deleteProject(@RequestParam("id") String id) {
        ProjectHttpResponseEntity projectHttpResponseEntity = new ProjectHttpResponseEntity();
        try {
            int res = projectDataService.deleteProject(id);
            if (res != 0) {
                projectHttpResponseEntity.setCode(Constans.EXIST_CODE);
            } else {
                projectHttpResponseEntity.setCode(Integer.toString(801));
            }
        } catch (Exception e) {
            projectHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return projectHttpResponseEntity;
    }

    @RequestMapping(value = "/modifyProject", method = RequestMethod.PUT, headers = "Accept=application/json")
    public ProjectHttpResponseEntity modifyProject(@RequestBody Map<String, Object> map) {
        ProjectHttpResponseEntity projectHttpResponseEntity = new ProjectHttpResponseEntity();
        try {
            projectDataService.modifyProject(map);
            projectHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            projectHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return projectHttpResponseEntity;
    }
}


class ProjectHttpResponseEntity implements Serializable {

    private String code;
    private Object projectList;

    private Object projectData;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getProjectList() {
        return projectList;
    }

    public void setProjectList(Object projectList) {
        this.projectList = projectList;
    }

    public Object getProjectData() {
        return projectData;
    }

    public void setProjectData(Object projectData) {
        this.projectData = projectData;
    }
}
