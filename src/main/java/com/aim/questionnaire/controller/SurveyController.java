package com.aim.questionnaire.controller;

import com.aim.questionnaire.common.Constans;
import com.aim.questionnaire.dao.SurveyTemplateMapper;
import com.aim.questionnaire.dao.entity.SurveyLinked;
import com.aim.questionnaire.dao.entity.SurveyTemplate;
import com.aim.questionnaire.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


@RestController
public class SurveyController {
    private SurveyTemplateMapper surveyTemplateMapper;

    @Autowired
    private SurveyService surveyService;

    @RequestMapping(value = "/addSurvey", method = RequestMethod.POST, headers = "Accept=application/json")
    public SurveyHttpResponseEntity addSurvey(@RequestBody SurveyTemplate surveyTemplateEntity) {
        SurveyHttpResponseEntity surveyHttpResponseEntity = new SurveyHttpResponseEntity();
        try {
            surveyService.addSurvey(surveyTemplateEntity);
            surveyHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            surveyHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return surveyHttpResponseEntity;
    }

    @RequestMapping(value = "/linkSurvey", method = RequestMethod.POST, headers = "Accept=application/json")
    public SurveyHttpResponseEntity linkSurvey(@RequestBody Map<String, Object> map) {
        SurveyHttpResponseEntity surveyHttpResponseEntity = new SurveyHttpResponseEntity();
        try {
            surveyService.linkSurvey(map);
            surveyHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            surveyHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return surveyHttpResponseEntity;
    }

    @RequestMapping(value = "/querySurveyList", method = RequestMethod.GET, headers = "Accept=application/json")
    public SurveyHttpResponseEntity
    querySurveyListLinked(@RequestParam(required = false, name = "projectId") String projectId) {
        SurveyHttpResponseEntity surveyHttpResponseEntity = new SurveyHttpResponseEntity();
        try {
            List res;
            if (projectId != null) {
                res = surveyService.querySurveyListLinked(projectId);
            } else {
                res = surveyService.querySurveyListTemplate();
            }
            surveyHttpResponseEntity.setSurveyList(res);
            surveyHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            surveyHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return surveyHttpResponseEntity;
    }

    //   @RequestMapping(value = "/querySurveyList", method = RequestMethod.GET, headers = "Accept=application/json")
    //   public SurveyHttpResponseEntity querySurveyListTemplate() {
    //       SurveyHttpResponseEntity surveyHttpResponseEntity = new SurveyHttpResponseEntity();
    //       try {
    //           List res = surveyService.querySurveyListTemplate();
    //           surveyHttpResponseEntity.setSurveyList(res);
    //           Integer success = 801;
    //           surveyHttpResponseEntity.setCode(success.toString());
    //       } catch (Exception e) {
    //           surveyHttpResponseEntity.setCode(Constans.EXIST_CODE);
    //       }
    //       return surveyHttpResponseEntity;
    //   }

    @RequestMapping(value = "/querySurveyById", method = RequestMethod.GET, headers = "Accept=application/json")
    public SurveyHttpResponseEntity
    querySurveyById(@RequestParam("surveyId") String surveyId,
                    @RequestParam(required = false, name = "projectId") String projectId) {
        SurveyHttpResponseEntity surveyHttpResponseEntity = new SurveyHttpResponseEntity();
        try {
            Object res = surveyService.querySurveyById(surveyId);
            surveyHttpResponseEntity.setSurveyData(res);
            surveyHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            surveyHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return surveyHttpResponseEntity;
    }

    @RequestMapping(value = "/modifySurvey", method = RequestMethod.PUT, headers = "Accept=application/json")
    public SurveyHttpResponseEntity modifySurvey(@RequestBody Map<String, Object> map) {
        SurveyHttpResponseEntity surveyHttpResponseEntity = new SurveyHttpResponseEntity();
        try {
            if (map.get("projectId") != null) {
                surveyService.modifySurveyLinked(map);
            } else {
                surveyService.modifySurveyTemplate(map);
            }
            surveyHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            surveyHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return surveyHttpResponseEntity;
    }

    @RequestMapping(value = "/deleteSurvey", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public SurveyHttpResponseEntity deleteSurvey(@RequestParam("surveyId") String surveyId) {
        SurveyHttpResponseEntity surveyHttpResponseEntity = new SurveyHttpResponseEntity();
        try {
            surveyService.deleteSurvey(surveyId);
            surveyHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            surveyHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return surveyHttpResponseEntity;
    }

    @RequestMapping(value = "/sendSurvey", method = RequestMethod.POST, headers = "Accept=application/json")
    public SurveyHttpResponseEntity sendSurvey(@RequestBody Map<String, Object> map) {
        SurveyHttpResponseEntity surveyHttpResponseEntity = new SurveyHttpResponseEntity();
        try {
            surveyService.sendSurvey(map);
            surveyHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            surveyHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return surveyHttpResponseEntity;
    }
}

class SurveyHttpResponseEntity implements Serializable {

    private String code;
    private Object surveyList;

    private Object surveyData;

    public SurveyHttpResponseEntity() {
    }

    public SurveyHttpResponseEntity(String code, Object surveyList, Object surveyData) {
        this.code = code;
        this.surveyList = surveyList;
        this.surveyData = surveyData;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getSurveyList() {
        return surveyList;
    }

    public void setSurveyList(Object surveyList) {
        this.surveyList = surveyList;
    }

    public Object getSurveyData() {
        return surveyData;
    }

    public void setSurveyData(Object surveyData) {
        this.surveyData = surveyData;
    }
}
