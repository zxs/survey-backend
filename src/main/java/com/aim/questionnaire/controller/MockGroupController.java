package com.aim.questionnaire.controller;

import com.aim.questionnaire.common.Constans;
import com.aim.questionnaire.service.LoginAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class MockGroupController {

    @RequestMapping(value = "/addGroup", method = RequestMethod.POST, headers = "Accept=application/json")
    public GroupHttpResponseEntity addGroup(@RequestBody Map<String, Object> map) {
        GroupHttpResponseEntity groupHttpResponseEntity = new GroupHttpResponseEntity();
        groupHttpResponseEntity.setCode(Integer.toString(801));
        return groupHttpResponseEntity;
    }

    @RequestMapping(value = "/queryGroupList", method = RequestMethod.GET, headers = "Accept=application/json")
    public GroupHttpResponseEntity queryGroupList(
            @RequestParam(required = false, name = "userId") String userId,
            @RequestParam(required = false, name = "tenantId") String tenantId,
            @RequestParam(required = false, name = "groupName") String groupName) {
        GroupHttpResponseEntity groupHttpResponseEntity = new GroupHttpResponseEntity();
        try {
            List list = new ArrayList();
            HashMap<String, Object> map = new HashMap<>();
            map.put("id", "00000000000000000000000000000000");
            map.put("userId", "11111111111111111111111111111111");
            map.put("groupName", "group0");
            map.put("groupDescription", "...");
            map.put("tenantId", "22222222222222222222222222222222");
            map.put("status", "good");
            map.put("deleted", 0);
            list.add(map);
            groupHttpResponseEntity.setGroupList(list);
            groupHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            groupHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return groupHttpResponseEntity;
    }

    @RequestMapping(value = "/modifyGroup", method = RequestMethod.PUT, headers = "Accept=application/json")
    public GroupHttpResponseEntity modifyGroup(@RequestBody Map<String, Object> map) {
        GroupHttpResponseEntity groupHttpResponseEntity = new GroupHttpResponseEntity();
        groupHttpResponseEntity.setCode(Integer.toString(801));
        return groupHttpResponseEntity;
    }


    @RequestMapping(value = "/deleteGroup", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public GroupHttpResponseEntity deleteGroup(@RequestParam("id") String id) {
        GroupHttpResponseEntity groupHttpResponseEntity = new GroupHttpResponseEntity();
        groupHttpResponseEntity.setCode(Integer.toString(801));
        return groupHttpResponseEntity;
    }
}

class GroupHttpResponseEntity implements Serializable {

    private String code;
    private Object groupList;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getGroupList() {
        return groupList;
    }

    public void setGroupList(Object groupList) {
        this.groupList = groupList;
    }
}
