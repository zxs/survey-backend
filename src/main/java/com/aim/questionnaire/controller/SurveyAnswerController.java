package com.aim.questionnaire.controller;

import com.aim.questionnaire.common.Constans;
import com.aim.questionnaire.dao.SurveyAnswerMapper;
import com.aim.questionnaire.dao.SurveyTemplateMapper;
import com.aim.questionnaire.dao.base.SurveyAnswerBaseMapper;
import com.aim.questionnaire.dao.entity.SurveyAnswer;
import com.aim.questionnaire.dao.entity.SurveyTemplate;
import com.aim.questionnaire.service.SurveyAnswerService;
import com.aim.questionnaire.service.SurveyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

@RestController
public class SurveyAnswerController {
    private SurveyAnswerMapper surveyAnswerMapper;

    @Autowired
    private SurveyAnswerService surveyAnswerService;

    @RequestMapping(value = "/answerSurvey", method = RequestMethod.POST, headers = "Accept=application/json")
    public SurveyAnswerHttpResponseEntity answerSurvey(@RequestBody Map<String,Object> map) {
        SurveyAnswerHttpResponseEntity surveyAnswerHttpResponseEntity = new SurveyAnswerHttpResponseEntity();
        try {
            surveyAnswerService.answerSurvey(map);
            surveyAnswerHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            surveyAnswerHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return surveyAnswerHttpResponseEntity;
    }


    @RequestMapping(value = "querySurveyResult", method = RequestMethod.GET, headers = "Accept=application/json")
    public SurveyHttpResponseEntity querySurveyResult(@RequestParam("id") String surveyId) {
        SurveyHttpResponseEntity surveyHttpResponseEntity = new SurveyHttpResponseEntity();
        try {
            Object res = surveyAnswerService.querySurveyResult(surveyId);
            surveyHttpResponseEntity.setSurveyList(res);
            surveyHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            surveyHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return surveyHttpResponseEntity;
    }

}

class SurveyAnswerHttpResponseEntity implements Serializable {

    private String code;
    private Object surveyResult;

    public SurveyAnswerHttpResponseEntity() {
    }

    public SurveyAnswerHttpResponseEntity(String code, Object surveyResult) {
        this.code = code;
        this.surveyResult = surveyResult;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getSurveyResult() {
        return surveyResult;
    }

    public void setSurveyResult(Object surveyResult) {
        this.surveyResult = surveyResult;
    }
}

