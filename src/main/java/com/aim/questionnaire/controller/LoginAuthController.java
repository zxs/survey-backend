package com.aim.questionnaire.controller;

import com.aim.questionnaire.common.Constans;
import com.aim.questionnaire.service.LoginAuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


@RestController
public class LoginAuthController {

    @Autowired
    private LoginAuthService loginAuthService;

    @RequestMapping(value = "/pwLogin", method = RequestMethod.POST, headers = "Accept=application/json")
    public LoginAuthHttpResponseEntity pwLogin(@RequestBody Map<String, Object> map) {
        LoginAuthHttpResponseEntity loginAuthHttpResponseEntity = new LoginAuthHttpResponseEntity();
        try {
            Map<String, Object> userData = loginAuthService.pwLogin((String) map.get("account"), (String) map.get("password"));
            if (userData == null) {
                loginAuthHttpResponseEntity.setCode(Constans.EXIST_CODE);
            } else {
                loginAuthHttpResponseEntity.setUserData(userData);
                loginAuthHttpResponseEntity.setCode(Integer.toString(801));
            }
        } catch (Exception e) {
            loginAuthHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return loginAuthHttpResponseEntity;
    }

    @RequestMapping(value = "/sendSms", method = RequestMethod.GET, headers = "Accept=application/json")
    public LoginAuthHttpResponseEntity
    sendSms(@RequestParam("phoneNumber") String phoneNumber,
            @RequestParam("purpose") String purpose) {
        LoginAuthHttpResponseEntity loginAuthHttpResponseEntity = new LoginAuthHttpResponseEntity();
        try {
            loginAuthService.sendSms(phoneNumber, purpose);
            loginAuthHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            loginAuthHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return loginAuthHttpResponseEntity;
    }

    @RequestMapping(value = "/smsLogin", method = RequestMethod.POST, headers = "Accept=application/json")
    public LoginAuthHttpResponseEntity smsLogin(@RequestBody Map<String, Object> map) {
        LoginAuthHttpResponseEntity loginAuthHttpResponseEntity = new LoginAuthHttpResponseEntity();
        try {
            String veriCode = (String) map.get("veriCode");
            String phoneNumber = (String) map.get("phoneNumber");
            Map<String, Object> userData = loginAuthService.smsLogin(veriCode, phoneNumber);
            loginAuthHttpResponseEntity.setUserData(userData);
            loginAuthHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            loginAuthHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return loginAuthHttpResponseEntity;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST, headers = "Accept=application/json")
    public LoginAuthHttpResponseEntity register(@RequestBody Map<String, Object> map) {
        LoginAuthHttpResponseEntity loginAuthHttpResponseEntity = new LoginAuthHttpResponseEntity();
        try {
            Boolean res = loginAuthService.register(map);
            if (res) {
                loginAuthHttpResponseEntity.setCode(Integer.toString(801));
            } else {
                loginAuthHttpResponseEntity.setCode(Constans.EXIST_CODE);
            }
        } catch (Exception e) {
            loginAuthHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return loginAuthHttpResponseEntity;
    }

    @RequestMapping(value = "/secRetrieval", method = RequestMethod.POST, headers = "Accept=application/json")
    public LoginAuthHttpResponseEntity secRetrieval(@RequestBody Map<String, Object> map) {
        LoginAuthHttpResponseEntity loginAuthHttpResponseEntity = new LoginAuthHttpResponseEntity();
        try {
            Boolean res = loginAuthService.secRetrieval(map);
            if (res) {
                loginAuthHttpResponseEntity.setCode(Integer.toString(801));
            } else {
                loginAuthHttpResponseEntity.setCode(Constans.EXIST_CODE);
            }
        } catch (Exception e) {
            loginAuthHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return loginAuthHttpResponseEntity;
    }


    @RequestMapping(value = "/smsRetrieval", method = RequestMethod.POST, headers = "Accept=application/json")
    public LoginAuthHttpResponseEntity smsRetrieval(@RequestBody Map<String, Object> map) {
        LoginAuthHttpResponseEntity loginAuthHttpResponseEntity = new LoginAuthHttpResponseEntity();
        try {
            Boolean res = loginAuthService.smsRetrieval(map);
            if (res) {
                loginAuthHttpResponseEntity.setCode(Integer.toString(801));
            } else {
                loginAuthHttpResponseEntity.setCode(Constans.EXIST_CODE);
            }
        } catch (Exception e) {
            loginAuthHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return loginAuthHttpResponseEntity;
    }
}


class LoginAuthHttpResponseEntity implements Serializable {

    private String code;
    private Object userData;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getUserData() {
        return userData;
    }

    public void setUserData(Object userData) {
        this.userData = userData;
    }
}
