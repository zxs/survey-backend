package com.aim.questionnaire.controller;

import com.aim.questionnaire.common.Constans;
import com.aim.questionnaire.service.TenantDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.Serializable;
import java.util.Map;

public class TenantDataController {

    @Autowired
    private TenantDataService tenantDataService;

    @RequestMapping(value = "/queryTenantList", method = RequestMethod.GET, headers = "Accept=application/json")
    public TenantHttpResponseEntity queryTenantList(@RequestParam(required = false,name = "phoneNumber")String phone_number,
                                                    @RequestParam(required = false,name = "account")String account) {
        TenantHttpResponseEntity tenantHttpResponseEntity = new TenantHttpResponseEntity();
        try {
            Object res = tenantDataService.queryTenantList(phone_number, account);
            tenantHttpResponseEntity.setTenantList(res);
            tenantHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            tenantHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return tenantHttpResponseEntity;
    }

    @RequestMapping(value = "/deleteTenant", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public TenantHttpResponseEntity deleteTenant(@RequestParam("id") String id){
        TenantHttpResponseEntity tenantHttpResponseEntity = new TenantHttpResponseEntity();
        try {
            int res = tenantDataService.deleteTenant(id);
            if (res != 0) {
                tenantHttpResponseEntity.setCode(Constans.EXIST_CODE);
            } else {
                tenantHttpResponseEntity.setCode(Integer.toString(801));
            }
        } catch (Exception e) {
            tenantHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return tenantHttpResponseEntity;
    }

    @RequestMapping(value = "/modifyTenant", method = RequestMethod.PUT, headers = "Accept=application/json")
    public TenantHttpResponseEntity modifyTenant(@RequestBody Map<String, Object> map) {
        TenantHttpResponseEntity tenantHttpResponseEntity = new TenantHttpResponseEntity();
        try {
            tenantDataService.modifyTenant(map);
            tenantHttpResponseEntity.setCode(Integer.toString(801));
        } catch (Exception e) {
            tenantHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return tenantHttpResponseEntity;
    }

}
class TenantHttpResponseEntity implements Serializable {
    private String code;

    private Object message;

    private Object tenantList;

    public TenantHttpResponseEntity() {
    }

    public String getcode(){return code;};

    public void setCode(String code){this.code = code;};

    public Object getMessage(){return message;};

    public void setTenantList(Object tenantList) {
        this.tenantList = tenantList;
    }
}

