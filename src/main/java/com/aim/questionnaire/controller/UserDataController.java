package com.aim.questionnaire.controller;

import com.aim.questionnaire.common.Constans;
import com.aim.questionnaire.dao.base.UserDataBaseMapper;
import com.aim.questionnaire.service.UserDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.Map;

@RestController
public class UserDataController {

    private UserDataBaseMapper userDataBaseMapper;

    @Autowired
    private UserDataService userDataService;

    @RequestMapping(value = "/userLogin", method = RequestMethod.POST, headers = "Accept=application/json")
    public UserDataHttpResponseEntity userLogin(@RequestBody Map<String, Object> map) {
        UserDataHttpResponseEntity userDataHttpResponseEntity = new UserDataHttpResponseEntity();
        try {
          Map<String,Object> res=  userDataService.userLogin((String)map.get("account"),(String)map.get("password"));
          if (res.size()==0){
              userDataHttpResponseEntity.setCode(Constans.EXIST_CODE);
          }
          else {
              Integer success = 801;
              userDataHttpResponseEntity.setCode(success.toString());
              userDataHttpResponseEntity.setUserData(res);
          }
        } catch (Exception e) {
            userDataHttpResponseEntity.setCode(Constans.EXIST_CODE);
        }
        return userDataHttpResponseEntity;
    }
}

class UserDataHttpResponseEntity implements Serializable {

    private String code;
    private Object userData;

    public UserDataHttpResponseEntity() {
    }
    public UserDataHttpResponseEntity(String code, Object userData) {
        this.code = code;
        this.userData = userData;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getUserData() {
        return userData;
    }

    public void setUserData(Object userData) {
        this.userData = userData;
    }
}
