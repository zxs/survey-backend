package com.aim.questionnaire.common;

public enum Role {
    ADMIN,
    TENANT,
    USER,
    REPLIER;
}